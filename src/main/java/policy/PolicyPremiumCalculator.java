package policy;

import policy.object.PolicyObject;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PolicyPremiumCalculator implements PremiumCalculator{

    public BigDecimal calculate(Policy policy){
        //It will calcualte summ for all insurance bonuses for customer
           return policy.getPolicyOvjects().stream().map(item->item.getSumInsured()).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
